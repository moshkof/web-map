import folium

map = folium.Map(location=[59.922853, 30.328823], zoom_start=10)

fg = folium.FeatureGroup(name="Моя карта")
fg.add_child(
    folium.Marker(location=[59.807615, 30.379665], popup='Привет, здесь живу я', icon=folium.Icon(color='green')))
fg.add_child(
    folium.Marker(location=[59.859455, 30.208591], popup='Здесь живет любимая!', icon=folium.Icon(color='red')))

map.add_child(fg)

map.save("map.html")
